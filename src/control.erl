%%%-----------------------------------------------------------------------------
%%% @author Max Morén <maxmoren@gmail.com>
%%% @doc
%%% Connection accepter. Handles incoming connections, starts workers and
%%% connects their sockets to them.
%%% @end
%%%-----------------------------------------------------------------------------

-module(control).

-behaviour(gen_fsm).

%%% API
-export([start/0, set_socket/2]).

%%% Callbacks
-export([init/1,
         handle_event/3,
         handle_sync_event/4,
         handle_info/3,
         terminate/3,
         code_change/4]).

%%% States
-export([idle/2]).

%%% Records
-record(state, {socket,
                username,
                cwd}).

%%%=============================================================================
%%% API functions
%%%=============================================================================

start() ->
    gen_fsm:start(?MODULE, no_args, []).

set_socket(Ref, Socket) ->
    gen_fsm:send_event(Ref, {set_socket, Socket}).

%%%=============================================================================
%%% State functions
%%%=============================================================================

idle({set_socket, Socket}, State) ->
    gen_tcp:send(Socket, <<"220 eFTP 1.0\r\n">>),
    inet:setopts(Socket, [{active, once}]),
    {next_state, idle, State#state{socket = Socket}};

idle(Message, State) ->
    error_logger:info_report([idle_unknown_message, {message, Message}]),
    {next_state, idle, State}.

%%%=============================================================================
%%% Callbacks
%%%=============================================================================

%% @private
init(_Args) ->
    {ok, idle, #state{cwd = "/"}}.

handle_event(Event, StateName, StateData) ->
    error_logger:info_report([event,
                              {state_name, StateName},
                              {state_data, StateData},
                              {event,      Event}]),
    ?MODULE:StateName(Event, StateData).

handle_sync_event(Event, From, StateName, StateData) ->
    error_logger:info_report([sync_event,
                              {state_name, StateName},
                              {state_data, StateData},
                              {from,       From},
                              {event,      Event}]),
    {next_state, StateName, StateData}.

%% @private
handle_info({tcp, Socket, Data}, StateName, StateData = #state{socket = Socket}) ->
    ?MODULE:StateName({data, Data}, StateData);
handle_info({tcp_closed, Socket}, StateName, StateData = #state{socket = Socket}) ->
    ?MODULE:StateName(closed, StateData);
handle_info(Message, StateName, StateData = #state{socket = Socket}) ->
    error_logger:info_report([info,
                              {state_name, StateName},
                              {state_data, StateData},
                              {message,    Message}]),
    inet:setopts(Socket, [{active, once}]),
    {next_state, StateName, StateData}.

code_change(_OldVsn, StateName, StateData, _Extra) ->
    {next_state, StateName, StateData}.

%% @private
terminate(_Reason, _StateName, _StateData) ->
    ok.
