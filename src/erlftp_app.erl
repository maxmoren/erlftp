%%%-----------------------------------------------------------------------------
%%% @author Max Morén <maxmoren@gmail.com>
%%% @doc
%%% FTP server application module.
%%% @end
%%%-----------------------------------------------------------------------------

-module(erlftp_app).

-behaviour(application).

%%% Callbacks
-export([start/2, stop/1]).

%%%=============================================================================
%%% Callback functions
%%%=============================================================================

start(_StartType, _StartArgs) ->
    erlftp_sup:start_link().

stop(_State) ->
    ok.
