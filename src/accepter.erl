%%%-----------------------------------------------------------------------------
%%% @author Max Morén <maxmoren@gmail.com>
%%% @doc
%%% Connection accepter. Handles incoming connections, starts workers and
%%% connects their sockets to them.
%%% @end
%%%-----------------------------------------------------------------------------

-module(accepter).

-behaviour(gen_server).

%%% API
-export([start_link/0]).

%%% Callbacks
-export([init/1,
         handle_call/3,
         handle_cast/2,
         handle_info/2,
         terminate/2,
         code_change/3]).

%%% Records
-record(state, {listener,
                acceptor}).

%%%=============================================================================
%%% API functions
%%%=============================================================================

start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, 2100, []).

%%%=============================================================================
%%% Callback functions
%%%=============================================================================

%% @private
init(Port) ->
    Opts = [binary, {packet, raw}, {reuseaddr, true},
            {keepalive, true}, {backlog, 128}, {active, false}],

    case gen_tcp:listen(Port, Opts) of
        {ok, ListenSocket} ->
            % Create first accepting process.

            {ok, Ref} = prim_inet:async_accept(ListenSocket, -1),
            {ok, #state{listener = ListenSocket,
                        acceptor = Ref}};
        {error, Reason} ->
            {stop, Reason}
    end.

%% @private
handle_call(Request, _From, State) ->
    {stop, {unknown_call, Request}, State}.

%% @private
handle_cast(_Msg, State) ->
    {noreply, State}.

%% @private
handle_info({inet_async, ListenSocket, Ref, {ok, ClientSocket}},
            State = #state{listener = ListenSocket,
                           acceptor = Ref}) ->
    try
        case set_sockopt(ListenSocket, ClientSocket) of
            ok              -> ok;
            {error, Reason} -> exit({set_sockopt, Reason})
        end,
        % New client connected - spawn a new process using the
        % simple_one_for_one supervisor.
        {ok, Pid} = control:start(),
        gen_tcp:controlling_process(ClientSocket, Pid),

        % Instruct the new FSM that it owns the socket.
        control:set_socket(Pid, ClientSocket),

        %% Signal the network driver that we are ready to accept another
        %% connection.
        case prim_inet:async_accept(ListenSocket, -1) of
            {ok,    NewRef} -> ok;
            {error, NewRef} -> exit({async_accept, inet:format_error(NewRef)})
        end,
        {noreply, State#state{acceptor = NewRef}}
    catch exit:Why ->
        error_logger:error_msg("Error in async accept: ~p.\n", [Why]),
        {stop, Why, State}
    end;

handle_info({inet_async, ListSock, Ref, Error},
            State = #state{listener = ListSock,
                           acceptor = Ref}) ->
    error_logger:error_msg("Error in socket acceptor: ~p.\n", [Error]),
    {stop, Error, State};

handle_info(Info, State) ->
    error_logger:info_report([info,
                              {state, State},
                              {info,  Info}]),
    {noreply, State}.

%% @private
terminate(_Reason, State) ->
    gen_tcp:close(State#state.listener),
    ok.

%% @private
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%%%=============================================================================
%%% Internal functions
%%%=============================================================================

set_sockopt(ListSock, CliSocket) ->
    true = inet_db:register_socket(CliSocket, inet_tcp),
    case prim_inet:getopts(ListSock, [active, nodelay, keepalive, delay_send,
                                      priority, tos]) of
        {ok, Opts} ->
            case prim_inet:setopts(CliSocket, Opts) of
                ok    -> ok;
                Error -> gen_tcp:close(CliSocket), Error
            end;
        Error ->
            gen_tcp:close(CliSocket), Error
    end.
