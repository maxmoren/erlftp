%%%-----------------------------------------------------------------------------
%%% @author Max Morén <maxmoren@gmail.com>
%%% @doc
%%% Top-level supervisor of FTP server.
%%% @end
%%%-----------------------------------------------------------------------------

-module(erlftp_sup).

-behaviour(supervisor).

%%% API
-export([start_link/0]).

%%% Callbacks
-export([init/1]).

-define(CHILD(I, Type), {I, {I, start_link, []}, permanent, 5000, Type, [I]}).

%%%=============================================================================
%% API functions
%%%=============================================================================

start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

%%%=============================================================================
%%% Callback functions
%%%=============================================================================

init([]) ->
    {ok, {{one_for_one, 5, 10}, [?CHILD(accepter, worker)]}}.

